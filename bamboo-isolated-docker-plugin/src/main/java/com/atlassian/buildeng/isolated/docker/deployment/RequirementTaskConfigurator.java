/*
 * Copyright 2016 - 2017 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.isolated.docker.deployment;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskRequirementSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.buildeng.isolated.docker.Constants;
import com.atlassian.buildeng.isolated.docker.lifecycle.BuildProcessorServerImpl;
import com.atlassian.buildeng.spi.isolated.docker.AccessConfiguration;
import com.atlassian.buildeng.spi.isolated.docker.Configuration;
import com.atlassian.buildeng.spi.isolated.docker.ConfigurationPersistence;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RequirementTaskConfigurator extends AbstractTaskConfigurator implements TaskRequirementSupport {

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = LoggerFactory.getLogger(RequirementTaskConfigurator.class);
    private final TextProvider textProvider;

    private RequirementTaskConfigurator(TextProvider textProvider) {
        this.textProvider = textProvider;
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, 
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);
        configMap.put(Configuration.TASK_DOCKER_IMAGE, params.getString(Configuration.TASK_DOCKER_IMAGE));
        configMap.put(Configuration.TASK_DOCKER_IMAGE_SIZE, params.getString(Configuration.TASK_DOCKER_IMAGE_SIZE));
        configMap.put(Configuration.TASK_DOCKER_EXTRA_CONTAINERS, 
                params.getString(Configuration.TASK_DOCKER_EXTRA_CONTAINERS));
        return configMap;
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.putAll(taskDefinition.getConfiguration());
        context.put(Configuration.TASK_DOCKER_IMAGE, 
                taskDefinition.getConfiguration().get(Configuration.TASK_DOCKER_IMAGE));
        context.put("imageSizes", BuildProcessorServerImpl.getImageSizes());
        context.put(Configuration.TASK_DOCKER_IMAGE_SIZE, 
                taskDefinition.getConfiguration().get(Configuration.TASK_DOCKER_IMAGE_SIZE));
        context.put(Configuration.TASK_DOCKER_EXTRA_CONTAINERS, 
                taskDefinition.getConfiguration().get(Configuration.TASK_DOCKER_EXTRA_CONTAINERS));
    }

    @Override
    public void populateContextForCreate(Map<String, Object> context) {
        super.populateContextForCreate(context);
        context.put("imageSizes", BuildProcessorServerImpl.getImageSizes());
        context.put(Configuration.TASK_DOCKER_IMAGE_SIZE, Configuration.ContainerSize.REGULAR);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        
        String v = params.getString(Configuration.TASK_DOCKER_EXTRA_CONTAINERS);
        validateExtraContainers(v, errorCollection);

        String image = params.getString(Configuration.TASK_DOCKER_IMAGE);
        if (StringUtils.isBlank(image)) {
            errorCollection.addError(Configuration.TASK_DOCKER_IMAGE, 
                    textProvider.getText("requirement.error.emptyImage"));
        } else if (image != null && !image.trim().equals(image)) {
            errorCollection.addError(Configuration.TASK_DOCKER_IMAGE, 
                    textProvider.getText("requirement.error.whitespaceImage"));
        }
        
        String size = params.getString(Configuration.TASK_DOCKER_IMAGE_SIZE);
        try {
            Configuration.ContainerSize val = Configuration.ContainerSize.valueOf(size);
        } catch (IllegalArgumentException e) {
            errorCollection.addError(Configuration.TASK_DOCKER_IMAGE_SIZE, 
                    "Image size value to be one of:" + Arrays.toString(Configuration.ContainerSize.values()));
        }
    }
    
    //TODO a bit unfortunate that the field associated with extra containers is hidden
    // the field specific reporting is not showing at all then. So needs to be global.
    public static void validateExtraContainers(String value, ErrorCollection errorCollection) {
        if (!StringUtils.isBlank(value)) {
            try {
                JsonElement obj = new JsonParser().parse(value);
                if (!obj.isJsonArray()) {
                    errorCollection.addErrorMessage("Extra containers json needs to be an array.");
                } else {
                    JsonArray arr = obj.getAsJsonArray();
                    arr.forEach((JsonElement t) -> {
                        if (t.isJsonObject()) {
                            Configuration.ExtraContainer v2 = ConfigurationPersistence.from(t.getAsJsonObject());
                            if (v2 == null) {
                                errorCollection.addErrorMessage("wrong format for extra containers");
                            } else {
                                if (StringUtils.isBlank(v2.getName())) {
                                    errorCollection.addErrorMessage("Extra container requires a non empty name.");
                                }
                                if (!v2.getName().matches("[a-z0-9]([\\-a-z0-9]*[a-z0-9])?")) {
                                    errorCollection.addErrorMessage("Extra container name should "
                                            + "be composed of lowercase letters, numbers and - character only");
                                }
                                if (StringUtils.isBlank(v2.getImage())) {
                                    errorCollection.addErrorMessage("Extra container requires non empty image.");
                                }
                                for (Configuration.EnvVariable env : v2.getEnvVariables()) {
                                    if (StringUtils.isBlank(env.getName())) {
                                        errorCollection.addErrorMessage(
                                                "Extra container requires non empty environment variable name.");
                                    }
                                }
                            }
                        } else {
                            errorCollection.addErrorMessage("wrong format for extra containers");
                        }
                    });
                }
            } catch (RuntimeException e) {
                errorCollection.addErrorMessage("Extra containers field is not valid json.");
            }
        }
    }

    // TODO eventually remove once we are sure noone is using the capability anymore.

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition) {
        Set<Requirement> requirementSet = Sets.newHashSet();
        Configuration config = AccessConfiguration.forTaskConfiguration(taskDefinition);
        if (config.isEnabled()) {
            requirementSet.add(new RequirementImpl(Constants.CAPABILITY_RESULT, true, ".*", true));
        }
        return requirementSet;
    }

}
